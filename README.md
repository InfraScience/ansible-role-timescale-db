Role Name
=========

A role that installs a TimeScaleDB instance. See <https://docs.timescale.com/self-hosted/latest/install/installation-linux/>

Role Variables
--------------

The most important variables are listed below:

``` yaml
timescale_db_ubuntu_repo: 'deb https://packagecloud.io/timescale/timescaledb/ubuntu/ $(lsb_release -c -s) main'
timescale_db_pkg: 'timescaledb-2-postgresql-{{ psql_version }}'
timescale_db_deb_pkgs: '{{ timescale_db_pkg }}'

timescale_db_el_pkgs:
  - '{{ timescale_db_pkg }}'
  - libpq5-devel

timescaledb_configuration_settings:
  - { name: 'timescaledb.last_tuned', value: "'{{ ansible_date_time.date }}T{{ ansible_date_time.time }}{{ ansible_date_time.tz_offset }}'", set: 'True' }
#  - { name: 'timescaledb.last_tuned_version', value: "'0.11.0'", set: 'True' }
  - { name: 'timescaledb.max_background_workers', value: '{{ ansible_processor_vcpus * 2 }}', set: 'True' }
  - { name: 'max_worker_processes', value: '{{ (ansible_processor_vcpus * 4) - 1 }}', set: 'True' }
  - { name: 'max_parallel_workers_per_gather', value: '{{ (ansible_processor_vcpus / 2) | int }}', set: 'True' }
  - { name: 'max_parallel_workers ', value: '{{ ansible_processor_vcpus }}', set: 'True' }
  - { name: 'default_statistics_target', value: '{{ (ansible_memtotal_mb / 16) | int }}', set: 'True' }
  - { name: 'random_page_cost', value: '1.1', set: 'True' }
  - { name: 'checkpoint_completion_target', value: '0.9', set: 'True' }
  - { name: 'max_locks_per_transaction', value: '{{ ((ansible_memtotal_mb / 512) * ansible_processor_vcpus) | int }}', set: 'True' }
  - { name: 'effective_io_concurrency', value: '200', set: 'True' }
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
